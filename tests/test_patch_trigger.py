"""Tests for triggers.patch_trigger."""
import unittest
from unittest import mock

from cki_lib import config_tree
from cki_lib.misc import utc_now_iso
from freezegun import freeze_time
import responses
import yaml

from tests import fakes
import triggers.patch_trigger as patch_trigger
from triggers.utils import SeriesData

json_submitter = {
    "id": 1234,
    "url": "https://patchwork.somelabs.aaa/api/people/1234/",
    "name": "Joe User",
    "email": "joeuser@redhat.com"
}


class TestLastEventAndSkippedSeriesRetrieval(unittest.TestCase):
    """Tests for patch_trigger.get_next_date_and_skipped_series()."""

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_get_last_event(self, mock_variables):
        """Verify get_last_event_and_skipped_series gets newest variables."""
        event_id = '1234'
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        mock_variables.side_effect = [
            # Don't use event_id here to see if it gets skipped correctly
            {'patchwork_url': 'patchwork',
             'skipped_series': '',
             'patchwork_project': 'pw_project'},
            {'event_id': event_id,
             'skipped_series': '',
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        # Create some fun pipelines
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        self.assertEqual(
            patch_trigger.get_last_event_and_skipped_series(project,
                                                            data),
            (int(event_id), [])
        )

    def test_no_event_id(self):
        """Verify Exception is raised if there is no event ID to retrieve."""
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )

        with self.assertRaises(Exception):
            patch_trigger.get_last_event_and_skipped_series(
                project, data
            )

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_different_project(self, mock_variables):
        """Verify correct data is returned with multiple sources.

        Sometimes more Patchwork instances or projects are used with the same
        branch.
        """
        event_id = '1234'
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        mock_variables.side_effect = [
            {'patchwork_url': 'patchwork_2',
             'event_id': 'non-parsable',
             'skipped_series': '',
             'patchwork_project': 'pw_project'},
            {'event_id': event_id,
             'skipped_series': '',
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        self.assertEqual(
            patch_trigger.get_last_event_and_skipped_series(project,
                                                            data),
            (int(event_id), [])
        )


class TestMissingCover(unittest.TestCase):
    """Tests for patch_trigger.is_cover_missing()."""

    @responses.activate
    def test_is_cover_missing_exception(self):
        """Verify an exception is raised on bad response from Patchwork."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        responses.add(responses.GET, patch_api_url, status=404)

        with self.assertRaises(Exception):
            patch_trigger.is_cover_missing(
                patchwork_url,
                SeriesData(patches=['http://patchwork.test/123',
                                    'http://patchwork.test/124'])
            )

    def test_cover_present(self):
        """Verify is_cover_missing() returns False if a cover is present."""
        self.assertFalse(
            patch_trigger.is_cover_missing('patchwork',
                                           SeriesData(cover='link'))
        )

    @responses.activate
    def test_get_patch_is_cover_missing(self):
        """Verify is_cover_missing() returns True if a cover is expected."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'

        responses.add(responses.GET,
                      patch_api_url,
                      status=200,
                      json={'headers': {'In-Reply-To': 'msgid-of-cover'}})

        self.assertTrue(patch_trigger.is_cover_missing(
            patchwork_url,
            SeriesData(patches=[patchwork_url + '/patch/123',
                                patchwork_url + '/patch/124'])
        ))

    @responses.activate
    def test_get_patch_cover_not_missing(self):
        """Verify False is returned if a cover is not expected."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'

        responses.add(responses.GET,
                      patch_api_url,
                      status=200,
                      json={'headers': {}})

        self.assertFalse(patch_trigger.is_cover_missing(
            patchwork_url,
            SeriesData(patches=[patchwork_url + '/patch/123',
                                patchwork_url + '/patch/124'])
        ))


class TestRetrySkippedSeries(unittest.TestCase):
    """Test cases for patch_trigger.retry_skipped()."""

    @mock.patch('triggers.patch_trigger.get_series_data')
    def test_normal_retry(self, mock_series_data):
        """Verify we attempt to retrieve data for missed series."""
        series_data = {'series': 'data'}
        mock_series_data.return_value = series_data

        self.assertEqual(patch_trigger.retry_skipped('patchwork', [123], 987),
                         [series_data])
        mock_series_data.assert_called_once()


class TestKickstartSeries(unittest.TestCase):
    """Test cases for patch_trigger.get_kickstart_series()."""

    @responses.activate
    def test_no_series(self):
        """Verify Exception is raised if there are no series."""
        patchwork_url = 'http://patchwork.test'
        series_endpoint = patchwork_url + '/api/series?'
        responses.add(responses.GET,
                      series_endpoint,
                      status=200,
                      json=[])

        with self.assertRaises(Exception):
            patch_trigger.get_kickstart_series(patchwork_url, 'project')

    @responses.activate
    def test_bad_response(self):
        """Verify Exception is raised if series can't be retrieved."""
        patchwork_url = 'http://patchwork.test'
        series_endpoint = patchwork_url + '/api/series?'
        responses.add(responses.GET, series_endpoint, status=500)

        with self.assertRaises(Exception):
            patch_trigger.get_kickstart_series(patchwork_url, 'project')

    @responses.activate
    @mock.patch('triggers.patch_trigger.get_patch_data')
    def test_valid_series(self, mock_data):
        """Verify correct data is returned if there are existing series."""
        patchwork_url = 'http://patchwork.test'
        events_endrepoint = patchwork_url + '/api/events?'
        series_endpoint = patchwork_url + '/api/series/123'
        mock_data.return_value = 'message_id', 'subject'

        responses.add(responses.GET,
                      events_endrepoint,
                      status=200,
                      json=[{'id': 988, 'payload': {'series': {'id': 123}}}])

        responses.add(responses.GET,
                      series_endpoint,
                      status=200,
                      json={'id': 123,
                            'submitter': json_submitter,
                            'patches': [{'web_url': 'url', 'date': 'today'}],
                            'cover_letter': {'mbox': 'mbox'}})

        expected_data = SeriesData(patches=['url'],
                                   subject='subject',
                                   cover='mbox',
                                   series_id=str(123),
                                   message_id='message_id',
                                   last_tested='988',
                                   submitter="joeuser@redhat.com")
        data = patch_trigger.get_kickstart_series(patchwork_url, 'project')
        self.assertEqual([expected_data], data)


class TestSeriesByEvent(unittest.TestCase):
    """Tests for patch_trigger.get_series_by_event()."""

    @responses.activate
    @mock.patch('triggers.patch_trigger.get_series_data')
    def test_get_series_by_event(self, mock_get_series_data):
        """Verify the functionality of get_series_by_event()."""
        patchwork_url = 'http://patchwork.test'
        get_series_data_return = {'patches': [123]}
        mock_get_series_data.return_value = get_series_data_return

        responses.add(
            responses.GET,
            patchwork_url + '/api/events',
            status=200,
            json=[{'id': 112, 'payload': {'series': {'id': 1}}},
                  {'id': 111}],
        )

        returned_data = patch_trigger.get_series_by_event(patchwork_url,
                                                          'project',
                                                          111)
        self.assertEqual(returned_data, [get_series_data_return])
        mock_get_series_data.assert_called_once()

    @responses.activate
    @mock.patch('triggers.patch_trigger.get_series_data')
    def test_no_duplicates(self, mock_get_series_data):
        """Verify we only trigger one pipeline per series.

        Sometimes extra patches arrive later. This causes a new "series
        complete" even to fire. We only want to test the series once.
        """
        patchwork_url = 'http://patchwork.test'
        get_series_data_return = SeriesData(patches=['123'],
                                            series_id='1',
                                            last_tested='113')
        mock_get_series_data.return_value = get_series_data_return

        responses.add(
            responses.GET,
            patchwork_url + '/api/events',
            status=200,
            json=[{'id': 113, 'payload': {'series': {'id': 1}}},
                  {'id': 112, 'payload': {'series': {'id': 1}}},
                  {'id': 111}],
        )

        returned_data = patch_trigger.get_series_by_event(patchwork_url,
                                                          'project',
                                                          111)
        self.assertEqual(returned_data, [get_series_data_return])
        mock_get_series_data.assert_called_once()

    @responses.activate
    def test_get_series_by_event_exception(self):
        """Verify an exception is raised on bad response from Patchwork."""
        events_url = 'http://patchwork.test/api/events'
        responses.add(responses.GET, events_url, status=404)

        with self.assertRaises(Exception):
            patch_trigger.get_series_by_event('patchwork', events_url)


class TestGetSeriesData(unittest.TestCase):
    """Tests for patch_trigger.get_series_data()."""

    @responses.activate
    @mock.patch('triggers.utils.get_emails_from_headers')
    @mock.patch('triggers.patch_trigger.get_patch_data')
    def test_get_series_data(self, mock_patch_data, mock_emails):
        """Verify the functionality of get_series_data."""
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/1'
        mock_emails.return_value = set()
        mock_patch_data.return_value = ('message_id', 'subject')
        # Verify everything works with undefined series name
        series_data = {'id': 123, 'name': None, 'received_all': True,
                       'submitter': json_submitter,
                       'cover_letter': None,
                       'patches': [
                           {'id': 123, 'name': '[patch]', 'web_url': 'url',
                            'submitter': json_submitter}
                       ]}
        expected_series = SeriesData(
            patches=['url'],
            emails=set(),
            last_tested='987',
            message_id='message_id',
            subject='subject',
            series_id=str(123),
            submitter="joeuser@redhat.com"
        )

        responses.add(responses.GET,
                      series_url,
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/123',
                      status=200,
                      json={'id': 123,
                            'series': [{'id': 1}],
                            'headers': {},
                            'submitter': json_submitter})

        return_value = patch_trigger.get_series_data(patchwork_url,
                                                     series_url,
                                                     987)
        self.assertEqual(return_value, expected_series)
        mock_patch_data.assert_called_once()
        mock_emails.assert_called_once()

    @responses.activate
    def test_get_series_data_exception(self):
        """Verify an exception is raised on bad responses."""
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/'

        responses.add(responses.GET, series_url + '1', status=400)
        # Check for exception with bad series retrieval
        with self.assertRaises(Exception):
            patch_trigger.get_series_data(patchwork_url,
                                          series_url + '1',
                                          987)

        series_data = \
            {'id': 123, 'name': '[series]', 'received_all': True,
             'submitter': json_submitter,
             'cover_letter': None,
             'patches': [
                 {'id': 123, 'name': '[patch]', 'web_url': 'url',
                  'submitter': json_submitter}
             ]}
        responses.add(responses.GET,
                      series_url + '2',
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/123',
                      status=500)
        # Check for exception with bad patch retrieval
        with self.assertRaises(Exception):
            patch_trigger.get_series_data(patchwork_url,
                                          series_url + '2',
                                          987)

    @responses.activate
    def test_get_series_skip_series(self):
        """Verify get_series_data skips series correctly.

        Series should be skipped if they contain one of PATCH_SKIP patterns in
        the name or aren't completed.
        """
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/1'
        series_data = {'id': 123, 'name': '[series]', 'received_all': False,
                       'submitter': json_submitter}

        responses.add(responses.GET, series_url, status=200, json=series_data)
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url,
                                                       987),
                         None)

        series_data = {'id': 123, 'name': '[ethtool]', 'received_all': True}
        responses.add(responses.GET, series_url, status=200, json=series_data)

        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url,
                                                       987),
                         None)

    @responses.activate
    @mock.patch('triggers.utils.get_emails_from_headers')
    @mock.patch('triggers.patch_trigger.get_patch_data')
    def test_get_series_skip_patches(self, mock_patch_data, mock_emails):
        """Verify get_series_data skips individual patches correctly.

        Patches from series should be skipped if they contain one of
        PATCH_SKIP patterns in the name.
        """
        mock_emails.return_value = set()
        mock_patch_data.return_value = ('message_id', 'subject')

        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/'
        series_data = {'id': 123, 'name': '[series]', 'received_all': True,
                       'submitter': json_submitter,
                       'cover_letter': {'mbox': 'cover_mbox'},
                       'patches': [
                           {'id': 123, 'name': '[iproute]', 'web_url': 'url',
                            'submitter': json_submitter}
                       ]}
        responses.add(responses.GET,
                      series_url + '1',
                      status=200,
                      json=series_data)

        # Series contain a single patch that should be skipped
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url + '1',
                                                       987),
                         None)

        series_data['patches'].append({'id': 124, 'name': 'kernelpatch',
                                       'web_url': 'url2',
                                       'submitter': json_submitter
                                       })
        responses.add(responses.GET,
                      series_url + '2',
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/124',
                      status=200,
                      json={'headers': {}})

        # Only the second patch data should be returned
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url + '2',
                                                       987),
                         SeriesData(patches=['url2'],
                                    last_tested='987',
                                    message_id='message_id',
                                    subject='subject',
                                    cover='cover_mbox',
                                    series_id=str(123),
                                    submitter="joeuser@redhat.com"))
        mock_patch_data.assert_called_once()


class TestGetPatchData(unittest.TestCase):
    """Tests for patch_trigger.get_patch_data()."""

    @responses.activate
    def test_patch_data_exception(self):
        """Verify an exception is raised on bad response."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        patch_web_url = patchwork_url + '/patch/123'

        responses.add(responses.GET, patch_api_url, status=400)
        with self.assertRaises(Exception):
            patch_trigger.get_patch_data(patchwork_url, patch_web_url)

    @responses.activate
    def test_patch_data_valid(self):
        """Verify get_patch_data works with valid data."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        patch_web_url = patchwork_url + '/patch/123'
        data = {'headers': {'Subject': '[subject]', 'Message-ID': '<ab@cd>'},
                'id': 123}

        responses.add(responses.GET, patch_api_url, status=200, json=data)
        self.assertEqual(patch_trigger.get_patch_data(patchwork_url,
                                                      patch_web_url),
                         ('<ab@cd>', '[subject]'))


class TestLoadTriggers(unittest.TestCase):
    """Tests for patch_trigger.load_triggers()."""

    @freeze_time("2019-01-01")
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_trigger.get_last_event_and_skipped_series')
    @mock.patch('triggers.patch_trigger.get_series_by_event')
    @mock.patch('triggers.patch_trigger.is_cover_missing')
    def test_set_values(self, mock_cover, mock_series, mock_event_series,
                        mock_commit):
        """Verify all required keys and values are set."""
        config_text = '{}'.format(
            'test_name:\n'
            '  name: name\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: main\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
            '  .report_rules:\n'
            '    - if: something\n'
            '      send_cc: someone\n'
            '      send_bcc: someone_else\n'
        )
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_cover.return_value = False
        mock_commit.return_value = 'baseline_commit'
        mock_event_series.return_value = (987, [])
        mock_series.return_value = [SeriesData(
            patches=['http://patchwork.url/patch/123'],
            emails=set(['name@email']),
            subject='subject',
            message_id='message_id',
            last_tested='988',
            series_id=str(123),
            cover='http://patchwork.url/cover/123/mbox',
            submitter="joeuser@redhat.com"
        )]

        expected_trigger = {
            'git_url': 'http://git.test/git/kernel.git',
            'patchwork_url': 'http://patchwork.url',
            'patchwork_project': 'patchwork-project',
            'name': 'name',
            'commit_hash': 'baseline_commit',
            'branch': 'main',
            'cki_pipeline_branch': 'test_name',
            'cki_project': 'username/project',
            'patch_urls': 'http://patchwork.url/patch/123/mbox/',
            'subject': 'Re: subject',
            'message_id': 'message_id',
            'event_id': '988',
            'cover_letter': 'http://patchwork.url/cover/123/mbox',
            'skipped_series': '',
            'title': 'Patch: name: subject',
            'submitter': "joeuser@redhat.com",
            'discovery_time': utc_now_iso(),
            'checkout_contacts': '["name@email"]',
            'report_rules': (
                '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]'
            )
        }

        triggers = patch_trigger.load_triggers(gitlab, config, False)
        self.maxDiff = None
        self.assertEqual(triggers, [expected_trigger])

        config_text += '  new_var: foo@bar.com\n'
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)
        expected_trigger['new_var'] = 'foo@bar.com'
        triggers = patch_trigger.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [expected_trigger])

    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_trigger.get_last_event_and_skipped_series')
    @mock.patch('triggers.patch_trigger.get_series_by_event')
    @mock.patch('triggers.patch_trigger.is_cover_missing')
    def test_missing_series(self, mock_cover, mock_series, mock_event_series,
                            mock_commit):
        """Verify newly missing series are correctly assigned.

        This includes not triggering a pipeline for them.
        """
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: main\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
        )
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_cover.side_effect = [True, False]
        mock_commit.return_value = 'baseline_commit'
        mock_event_series.return_value = (987, [])
        mock_series.return_value = [
            SeriesData(patches=['http://patchwork.url/patch/123'],
                       emails=set(['name@email']),
                       subject='subject',
                       message_id='message_id',
                       last_tested='989',
                       series_id=str(124),
                       submitter=json_submitter),
            SeriesData(patches=['http://patchwork.url/patch/124'],
                       emails=set(['name2@email']),
                       subject='subject2',
                       message_id='message_id2',
                       last_tested='988',
                       series_id=str(123),
                       submitter=json_submitter)
        ]

        triggers = patch_trigger.load_triggers(gitlab, config, False)
        self.assertEqual(len(triggers), 1)
        self.assertEqual('123', triggers[0]['skipped_series'])

    @freeze_time("2019-01-01")
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_trigger.get_kickstart_series')
    def test_kickstart(self, mock_series, mock_commit):
        """Verify the kickstart creates correct trigger."""
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: main\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
        )
        config = yaml.safe_load(config_text)
        config = config_tree.process_config_tree(config)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_commit.return_value = 'baseline_commit'
        mock_series.return_value = [
            SeriesData(patches=['http://patchwork.url/patch/123'],
                       emails=set([]),
                       subject='subject',
                       message_id='message_id',
                       last_tested='988',
                       series_id=str(123),
                       submitter="joeuser@redhat.com"),
        ]

        expected_trigger = {
            'git_url': 'http://git.test/git/kernel.git',
            'patchwork_url': 'http://patchwork.url',
            'patchwork_project': 'patchwork-project',
            'name': 'test_name',
            'commit_hash': 'baseline_commit',
            'branch': 'main',
            'cki_pipeline_branch': 'test_name',
            'cki_project': 'username/project',
            'patch_urls': 'http://patchwork.url/patch/123/mbox/',
            'subject': 'Re: subject',
            'message_id': 'message_id',
            'event_id': '988',
            'cover_letter': '',
            'skipped_series': '',
            'title': 'Patch kickstart: test_name: subject',
            'submitter': "joeuser@redhat.com",
            'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger',
            'checkout_contacts': '[]',
            'discovery_time': utc_now_iso()
        }

        triggers = patch_trigger.load_triggers(gitlab, config, True)
        self.maxDiff = None
        self.assertEqual(triggers, [expected_trigger])
